<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mon CV</title>
</head>
<body>
    <h1>Moussa Diop</h1>
    <h3> Rufisque Dakar Moudiop123@gmail.com </h3>
    <br>
    <img src="https://web.facebook.com/photo?fbid=2201786916547627&set=a.100499310009742" alt="mon-profil">
    <br>
    <p>
        Aller directement à la partie traitant de :<br />
        <a href="#Formations">Formations</a><br />
        <a href="#Expériences">Expériences</a><br />
        <a href="#Competences">Competences</a><br />
    </p>
    <br>
    <h2 id="Formations">Formations</h2>
    <ul>
        <li>2008-2009 Brevet de fin d’étude moyenne (BFEM</li>
        <li>2011-2012 : Baccalauréat Série L2 </li>
        <li>2013-2014 : Faculté des sciences juridiques et politiques à l’université <br>
            Cheikh Anta Diop de Dakar (UCAD)</li>
        <li>2017-2018 Licence 1 en formation informatique généraliste</li>
        <li>2018-2019 Licence 2 en formation informatique généraliste</li>
    </ul>
    <br> <br>
    <h2 id="Expériences">Expériences </h2>
    <ul>
        <li>2014-2016: Gérant multiservices et money transfert d'argent à Dakar </li>
        <li>Cyber </li>
        <li>Transfert d'argent</li>
        <li>Traitement texte</li>
        <li>Vente de matérielles informatiques</li>
    </ul>
    <br> <br>
    <h2 id="Expériences">Competences</h2>
    <ul>
        <li> Bureautique</li>
        <p> Word Excel Powerpoint Google docs
        <li>Maintenance</li>   
        <p>Software Hardware  
        <li>Infographie </li> 
        <p>Photoshop Illustrator (débutant) In design  
        <li> Développement web </li> 
         <p>HTLM5 CSS3 Wordpress
    </ul>
    <br><br>
    <h2>Langues</h2>
    <ul>
        <li>Français : Très bien </li>
        <li>Anglais : intermédiaire</li>
        <li>Wolof : très bien</li>
        <li>Pulaar : bien</li>
    </ul>
</body>
</html>